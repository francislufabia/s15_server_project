let express = require('express');
const app = express();
// range of port 0 - 65536

let bodyParser = require('body-parser')
//the body-parser above is used as an alternative to express.json and express.url
//you dont need this if you us lines 49 below.

// lets acquire our env file and configure it's environment variables
require('dotenv').config() //this is the part where we load our environment variables in one line
// lets refactor our entrypoint to properly secure all sensitive information
// save the environment variables as constants
const port = process.env.PORT;
const connectionString = process.env.DB_CONNECTION_STRING;



// STEP: Connect to MongoDB Atlas
	//Acquire the needed dependencies to connect to MongoDB Atlas (Mongoose)
let mongoose = require('mongoose');
// STEP: Connect to mongoDB atlas via mongoose;
mongoose.connect(connectionString,{useNewUrlParser: true, useUnifiedTopology: true})
// Whenever you are executing a certain procedure/method, the outcome can always be either of the following: "success" or "failure"
// We want to get a notification for the connection success or failure.
let db = mongoose.connection; //this property describes the connection status
// We are going to use an eventHandler to identify the proper response.
// if the connection is rejected (failure), let's display the output in the console.
// on() method attaches one or more event handlers for the selected elements and child elements.
// on() method has two parameters: ('event', response 
// bind() creates a new function that, when called, has its "this" keyword to set to the provided value
// bind () method has two functions
db.on('error', console.error.bind(console, 'connection error:')); //the second parameter will display an error message detailing what went wrong.
// now, we will create a logic that will give the proper response if the connectio has been granted.
// once () is provided by NODE. Sometimes, you want your application to respond to any type of event only one time. we use once for that.
// once () has two parameters ('event', response)
db.once('open', () => console.log("We are connected to our cloud database: MongoDB Atlas"));


// Data Modeling - Creating the structure of the data
// Plan out how the information will be stored inside the database.

// schema -> blue print of the data
// build a schema for our tasks


//!!! Important so POSTMAN can be used
// the lines below is used to handling json request.
//the express.json() was called as a middleware in our app when we used the script below
app.use(express.json());
//the script below in form handling data
//express.urlencoded() is called as a middleware when we use it in the script below
app.use(express.urlencoded({extended:true})) 
//When and why we need express.url and express.json?
//you need both express.json and express.url for post and put requests
//because in both these requests, you are sending data in the form of some data object to the server and you are asking the server to accept/store that information/data, which is enclosed in the request body.
//alternative for express.url and express.json?
//yes, through installing an alternative that uses a body-parser.
//make sure you installed body-parser npm install body-parser in the terminal.
// app.use(bodyParser.json())
// app.use(bodyParser.urlencoded({extended:true}))

const taskBlueprint = new mongoose.Schema({
	name: String,  //this will describe the name of the task
	status:{
		type: String,
		default: 'pending'
	} //this will describe the status of task (done, ongoing, etc)
}) //by inserting the object inside the schema constructor, you will be able to instantiate this as the schema for the document


//compile our schema and convert it to a model
//model -> a class used in constructing a document (in context of mongoose)
const Task = mongoose.model('Task',taskBlueprint)
//the above argument (Task) is the singular name of the collection your model is for. Must be capital!!
//Mongoose automatically looks for the plural, lowercased version of the model name
//'Task' with the string decribes the name of the collection
// Task without the string describtes the name of the model
// an instance of a model is called "document"


//Define a schema for the users
const userSchema= new mongoose.Schema({
	username: String,
	email: String,
	password: String,
	//a feature of our project is that a user can be assigned multiple tasks
	tasks: [taskBlueprint]
})
const User = mongoose.model('User',userSchema)


//Create our first HTTP request
//lets create/register a new user inside our database
app.post('/users', (req, res) => {

//New task: create a logic that will validate the input of our users to make sure that there will be no double entries of data
//Syntax for $or:
//{$or [{expression1}, {expresssion2}]}
	//lets create a logic that will allow us to chck for duplicates for username and email
	User.find({$or: [{username: req.body.username}, {email: req.body.email}]}, (findErr, duplicates) => {
		//lets create a subfunction that will give us a feedback if the find() was successful or failure
		if(findErr) return console.error(findErr);
		//what will happen if a duplicate was found?
		if(duplicates.length > 0){
			//if condition was met, we will display a status code 403-forbidden
			return res.status(403).json({
				message: `Duplicates found. Kindly select a different username or email`
			})
		} else {
			//the body of this code will instantiate a new user "object" with properties derived from the request body
			let newUser = new User({
				username: req.body.username,
				email: req.body.email,
				password: req.body.password,
				// task property is initially an empty array
				tasks: []
			})
			//after caputuring the data from the request of the user, then its time to store it inside the database
			//the parameters below inside the save method will describe what will happen upon 'failure or success' in the request
			newUser.save((saveErr, newUser) => {
				//lets create a control structure that will determine the response of the server
				if (saveErr) return console.error(saveErr); //failure
				return res.status(201).json({
					message: `User with the username of ${newUser.username} sucessfully registered`,
					data: {
						username: newUser.username,
						email: newUser.email
					}
				})		
			}) //success
		}
	})
})

//lets create a request to show/display a particular user.
app.get('/users/:id', (req, res) => {
	//this will describe the action/procedure that will happen when the request is made.
	//first param (req.params.id), target/capture the data from the request
	//2nd param () create a function that will describe what will happen upon sucess or failure
	User.findById(req.params.id, (error, user) => {
		if(error) return console.log(error);
		return res.status(200).json({
			message: "User retrieved successfully",
			data: {
				username: user.username,
				email: user.email
			}
		})
	})
})

//lets create a request that will allow us to insert a task inside the tasks array
app.post('/users/:userId/tasks', (req, res) => {
	//find the user who will be added a new task
	User.findById(req.params.userId, (err, user) => {
		if(err) return console.error(err);
		//what if the user currently has no tasks?
		if(user.tasks === []){
			//add the new task inside a subdocument inside the tasks array
			user.tasks.push({
				name: req.body.name
			});
			user.save((saveErr, modifiedUser) => {
				if(saveErr) return console.error(saveErr);
				return res.status(200).json({
					message: `${user.tasks[0].name} added to the task list`,
					data: modifiedUser.tasks[0]
				})
			})
		} else {
			user.tasks.push({
				name: req.body.name
			});
			user.save((saveErr, modifiedUser) => {
				if(saveErr) return console.error(saveErr);
				return res.status(200).json({
					message: `${modifiedUser.tasks[modifiedUser.tasks.length-1].name} added to the task list`
				})
			})
		}
	})
})

//lets create a request which will allow us to delete an entry entry inside the database
//delete a task from a user
app.delete('/users/:userId/tasks/:targetTask', (req, res) => {
	User.findById(req.params.userId, (findErr, user) => {
		//the line above will describe what will happen upon searching the desired user
		if(findErr) return console.error(findErr);
		//what will now happen we successfylly found the user
		user.tasks.id(req.params.targetTask).remove() //the remove method will remove the selected element
		//once removed, its time to save the changes in our records.
		user.save((saveErr, modifiedUser) => {
			if(saveErr) return console.error(saveErr);
			return res.status(200).json({
				message: `Task with id of ${req.params.targetTask} is deleted successfully`,
				data: modifiedUser.tasks
			})
		})
	})
})



//Finish CRUD operations using Update 
//we will create route/request for us to update a specific task from a selected/specific user
app.put('/users/:userId/tasks/:taskId', (req, res) => {
	User.findById(req.params.userId, (findErr, user) => {
		//create a control structure that will allow us to determine the proper response of our method
		if(findErr) return console.error(findErr);
		let task = user.tasks.id(req.params.taskId)
		//create another control structure.
		if (task === null) {
			return res.status(404).json({
				message: `Task with id ${req.params.taskId} cannot be found!`
			}) //client errort, Not Found!
		} else {
			task.name = req.body.name		
			task.status = req.body.status
			//its time now to save the changes to our user.
			user.save((saveErr, updatedUser) => {
				if(saveErr) return console.log(saveErr);
				return res.status(200).json({
					message: `Task with id ${req.params.taskId} updated successfully`,
					data: task
				})
			})
		}
	}) //findById is to know the input of the user
})









//the 'app' is our backend (express)
//database is MongboDB, to access our database we used mongoose
// post is the HTTP method, /users is the endpoint name








// to get a response when the server start, add this script:
// 2 parameters for app.get(route, callback)
// route is where we state/describe the destination location of the app
// callback also has two paramaters, it tells what will happen after entering 
		// (request and response)
app.get('/', function(req, res) {
	res.send(`To Do App on port ${port}, server port`)
})

// res.send() takes an object as input and sents it to the requesting client


// next step
// create a logic that will allow us to listen/determine whether the connection has been granted or was sucessful
app.listen(port, () => console.log(`You got served on port ${port}`));

// app listen() finds and listens for connections in the specified host and port




